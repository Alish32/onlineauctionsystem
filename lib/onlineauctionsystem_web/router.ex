defmodule OnlineauctionsystemWeb.Router do
  use OnlineauctionsystemWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", OnlineauctionsystemWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index

    get "/items", ItemController, :auctions

    get "items/details", ItemController, :details
  end

  # Other scopes may use custom stacks.
  # scope "/api", OnlineauctionsystemWeb do
  #   pipe_through :api
  # end

  scope "/api", OnlineauctionsystemWeb do
    pipe_through :api
    post "/items", ItemController, :search
  end

  scope "/api", OnlineauctionsystemWeb do
    pipe_through :api
    post "/items/update", ItemController, :update_price
  end
end
