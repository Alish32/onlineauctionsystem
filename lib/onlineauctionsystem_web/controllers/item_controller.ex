defmodule OnlineauctionsystemWeb.ItemController do
    use OnlineauctionsystemWeb, :controller
    alias Onlineauctionsystem.Repo
    alias Onlineauctionsystem.Auction.Item
  
    import Ecto.Query

    def auctions(conn, params) do
      conn
        |> assign(:as, params["asd"])
        |> render "auctions.html"
    end

    def details(conn, params) do
      conn
        |> assign(:description, params["description"])
        |> assign(:price, params["price"])
        |> assign(:closing_date, params["closing_date"])
        |> render "details.html"
    end

    def search(conn, params) do
      items = from(
        from l in Item
      ) |> Repo.all([])
      items = items |> Enum.map(fn item -> %{
                id: item.id,
                description: item.description,
                price: item.price,
                closing_date: item.closing_date
              } end)
      conn
        |> put_status(200)
        |> json(%{items: items})
    end

    def update_price(conn, params) do
      from(
          from t in Item,
          where: t.id == ^params["id"],
          update: [set: [price: ^params["bid"] ]]
      ) |> Repo.update_all([])
      conn
      |> redirect(to: "/items")
    end

  end
  