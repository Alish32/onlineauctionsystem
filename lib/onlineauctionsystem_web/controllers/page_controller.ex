defmodule OnlineauctionsystemWeb.PageController do
  use OnlineauctionsystemWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
