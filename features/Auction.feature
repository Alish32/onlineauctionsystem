Feature: Select the auction item
  As a user
  Such that I want to buy item
  I want to suggest a suitable bid

  Scenario: Select item for auction
    Given the following items are available
          | Description | Price	 | closing_date |
          | Roller Derby | 29 | 29/01/2019 |
          | Chogo Bullet | 2599 | 26/10/2019 |
    And I want to give suitable suggestion
    And I open project web page
    And I select the item which i want to buy
    When I submit the request
    Then I should redirected to a details page

  Scenario: Give bid suggestion (with succes)
    Given the following item is available
        | Description | Price	 | closing_date |
        | Roller Derby | 29 | 29/01/2019 |
        | Chiago Bullet | 59 | 26/10/2019 |
    And I selected "Chiago Bullet" and  want to place bid
    And I am in details page
    And I enter the 62.00 as a bid value
    When I submit the "Place bid button"
    Then I should redirected to autcomes page

    Scenario: Give bid suggestion (with error)
    Given the following item is available
        | Description | Price	 | closing_date |
        | Chiago Bullet | 59 | 26/10/2019 |
        | Riedeli Dart | 106 | 30/01/2019 |
    And I selected "Riedeli Dart" and  want to place bid
    And I am in details page
    And I enter the 50.00 as a bid value
    When I submit the "Place bid button"
    Then I should get a message "Bid is equal or less than current price"
