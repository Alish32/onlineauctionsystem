defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers

  alias Onlineauctionsystem.{Repo,Auction.Item}

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(Onlineauctionsystem.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(Onlineauctionsystem.Repo, {:shared, self()})
    %{}
  end
  scenario_finalize fn _status, _state ->
    Ecto.Adapters.SQL.Sandbox.checkin(Onlineauctionsystem.Repo)
    # Hound.end_session
  end

  given_ ~r/^the following items are available$/, fn state, %{table_data: table} ->
    IO.puts "========================================================"
    IO.inspect table
    IO.puts "========================================================"
    table
    |> Enum.map(fn item -> Item.changeset(%Item{}, item) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^I want to give suitable suggestion"(?<id>[^"]+)"$/,
  fn state, %{} ->
    {:ok, state |> Map.put()}
  end

  and_ ~r/^I open project web page$/, fn state ->
    navigate_to "/items"
    {:ok, state}
  end

  and_ ~r/^I select the item which i want to buy$/, fn state ->
    fill_field({:id, "id"}, state[:id])
    {:ok, state}
  end

  when_ ~r/^I submit the request$/, fn state ->
    click({:id, "submit_button"})
    {:ok, state}
  end

  then_ ~r/^I should redirected to a details page$/, fn state ->
    navigate_to "/items/details"
    {:ok, state}
  end
end
