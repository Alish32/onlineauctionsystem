# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Onlineauctionsystem.Repo.insert!(%Onlineauctionsystem.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Onlineauctionsystem.{Repo, Auction.Item}

[%{description: "Roller Derby", price: "29", closing_date: "29/01/2019"},
 %{description: "Chicogo Bullet", price: "59", closing_date: "26/10/2019"},
 %{description: "Riedeli Dart", price: "106", closing_date: "30/01/2019"},]
|> Enum.map(fn item_data -> Item.changeset(%Item{}, item_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)
