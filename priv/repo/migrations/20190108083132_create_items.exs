defmodule Onlineauctionsystem.Repo.Migrations.CreateItems do
  use Ecto.Migration

  def change do
    create table(:items) do
      add :description, :string
      add :price, :string
      add :closing_date, :string

      timestamps()
    end

  end
end
